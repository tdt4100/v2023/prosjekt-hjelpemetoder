package exampleproject;

import javafx.fxml.FXML;
import javafx.scene.text.Text;

public class PopupController {

    // Denne controlleren fungerer som en normal controller, men er kun for den
    // nylig dannede popupen! Legg merke til at controller på endres i
    // SceneBuilder/Popup.fxml for at denne skal bli valgt

    @FXML
    private Text title;

    // Vi kan kalle metoder i denne klassen fra der vi åpner popupen!
    public void doSomething(String message) {
        System.out.println("Vi har fått en melding: " + message);

        // I disse metodene kan vi gjøre endringer også!
        this.title.setText(message);
    }

    @FXML
    public void handleButtonClick() {
        System.out.println("Jeg er knappen i popupen!");
    }
}
